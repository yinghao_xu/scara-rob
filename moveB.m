function mB = moveB(alpha, power)
    %{
    COM_CloseNXT all;
    % Open
    h=COM_OpenNXT();
    % Default value
    COM_SetDefaultNXT(h);
    %}
    %alpha = -alpha;
    
    power = min(100,power);
    power = max(5,power);
    
    mB = NXTMotor('B');
    mB.Power=-power;
    if alpha < 0 
        mB.Power = power;
    end
    mB.TachoLimit= round(abs(alpha)*33.6);
    mB.ActionAtTachoLimit= 'brake';
    %{
    mB.SendToNXT();
    mB.WaitFor();
    %}
end