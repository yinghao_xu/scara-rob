%scara
clear all
%close all
clc

pasos = 10;

N(1) = Link([0 0 10.5 0 0]);
N(2) = Link([0 0 19.2 pi 0]);

sc = SerialLink(N, 'name', 'scara');
sc.base = transl(21, 0, 0);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculo dese joins [0 0] hasta P5
o = [0 0];
j = transl(35.31,5,0);
qf = mod(sc.ikine(j, o, [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3 ),2*pi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%movimiento de P5 a P4 en linea recta
Ti = j;
Tf = transl(37.91,23.89,0);

ct = ctraj(Ti, Tf, pasos);

qt = zeros(pasos*4,2);
qt(1,:) =  mod(sc.ikine(ct(:,:,1), o, [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3 ),2*pi);
for i = 2 : pasos
    qt(i,:) = mod(sc.ikine(ct(:,:,i), qt(i-1,:), [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3),2*pi);
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%movimiento de P4 a P3 en linea recta
Ti = Tf;
Tf = transl(21,19.35,0);
offset = pasos;
ct = ctraj(Ti, Tf, pasos);

qt(offset+1,:) =  mod(sc.ikine(ct(:,:,1), o, [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3 ),2*pi);
for i = 2 : pasos
    qt((offset+i),:) = mod(sc.ikine(ct(:,:,i), qt((offset+i-1),:), [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3),2*pi);
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%movimiento de P3 a P2 en linea recta
Ti = Tf;
Tf = transl(8.5,25.95,0);
offset = pasos*2;
ct = ctraj(Ti, Tf, pasos);

qt(offset+1,:) =  mod(sc.ikine(ct(:,:,1), o, [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3 ),2*pi);
for i = 2 : pasos
    qt((offset+i),:) = mod(sc.ikine(ct(:,:,i), qt((offset+i-1),:), [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3),2*pi);
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%movimiento de P2 a P1 en linea recta
Ti = Tf;
Tf = transl(3.7,10.95,0);
offset = pasos*3;
ct = ctraj(Ti, Tf, pasos);

qt(offset+1,:) =  mod(sc.ikine(ct(:,:,1), o, [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3 ),2*pi);
for i = 2 : pasos
    qt((offset+i),:) = mod(sc.ikine(ct(:,:,i), qt((offset+i-1),:), [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3),2*pi);
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



COM_CloseNXT all;
h=COM_OpenNXT();
COM_SetDefaultNXT(h);

%movimiento al p5
moveto(o, qf(1), qf(2))

%hago todos los movimientos acumulados en qt
for i = 2 : 4*pasos
    moveto([qt(i-1,1) qt(i-1,2)],qt(i,1),qt(i,2));
end;

moveto([qt(i,1) qt(i,2)], o(1), o(2));

COM_CloseNXT(h);