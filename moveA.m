function mA = moveA(alpha, power)
    %{
    COM_CloseNXT all;
    % Open
    h=COM_OpenNXT();
    % Default value
    COM_SetDefaultNXT(h);
    %}
    power = min(100,power);
    power = max(5,power);

    mA = NXTMotor('A');
    mA.Power=power;
    if alpha < 0 
        mA.Power = -power;
    end
        
    mA.TachoLimit= round(abs(alpha)*7);
    mA.ActionAtTachoLimit= 'brake';
    
    %{    
    mA.SendToNXT();
    mA.WaitFor();
    %}
end