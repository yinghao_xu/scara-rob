%scara
clear all
%close all
clc

pasos = 20;

N(1) = Link([0 0 10.5 0 0]);
N(2) = Link([0 0 18 pi 0]);

sc = SerialLink(N, 'name', 'scara');
sc.base = transl(21, 0, 0);


sc.plot([0,0])

pause

%j = transl(1,1,0);
j = transl(30,1,0);
kine = sc.ikine(j,[pi/2 pi/2], [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3)
qf = mod(kine,2*pi)
%sc.plot(sc.ikine(j,[0 0], [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3 ))
sc.plot(qf)
