function [] = moveto(pos, newA, newB)
    
    offsetA = newA-pos(1);
    offsetB = newB-pos(2);

    if (pos(1) < newA)
        if (newA > (3*pi/2) && pos(1) < (3*pi/2))
            offsetA = -abs((2*pi)-abs(offsetA));
        end
    elseif (pos(1) < 2*pi && pos(1) > 3*pi/2 && newA < pi )
        offsetA = abs((2*pi)-abs(offsetA));
    end

    
    if (pos(2) < newB)
        if (pos(2) < pi && newB > pi )
            offsetB = -abs((2*pi)-abs(offsetB));
        end
    elseif (pos(2) > pi && newB < pi)
        offsetB = abs((2*pi)-abs(offsetB));
    end

    %offsetA
    %offsetB
        
    %%{
    %Control per evitar enviar zeros
    alpha = offsetA*(360/(2*pi));
    beta = offsetB*(360/(2*pi));
    A = 15;
    B = round(15*((abs(beta)*33.6)/(abs(alpha)*7)));
    if (B > 100)
        B = 100;
        A = round(100*((abs(alpha)*7)/(abs(beta)*33.6)));
    end
    
    if offsetA ~= 0
        mA = moveA(alpha,A);
    end
    if offsetB ~= 0
        mB = moveB(beta,B);
    end
    if offsetA ~= 0
        mA.SendToNXT();
    end
    if offsetB ~= 0
        mB.SendToNXT();
    end
    
    if offsetA ~= 0
        mA.WaitFor();
    end
    if offsetB ~= 0
        mB.WaitFor();
    end
    % fi control de zeros
    %%}
end