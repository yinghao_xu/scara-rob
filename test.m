%scara
clear all
close all
clc

pasos = 20;

N(1) = Link([0 0 10.5 0 0]);
N(2) = Link([0 0 18 pi 0]);

sc = SerialLink(N, 'name', 'scara');
sc.base = transl(21, 0, 0);

T = sc.fkine([0 0]);

j = transl(1,1,0);
qf = sc.ikine(j,[0 0], [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3 );

sc.plot(qf)


sc.plot([(pi+pi/4) 0])
COM_CloseNXT all;
h=COM_OpenNXT();
COM_SetDefaultNXT(h);

moveto([0 0], (pi+pi/4), 0);

COM_CloseNXT(h);








%{
Ti = sc.fkine([0 0]);
Tf = sc.fkine([pi -pi/2]);
%}
%{
ct = ctraj(Ti, Tf, pasos);

qt = zeros(pasos,2);
qt(1,:) = sc.ikine(ct(:,:,1), [0 0], [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3 );
for i = 2 : pasos
    qt(i,:) = sc.ikine(ct(:,:,i), qt(i-1,:), [1 1 0 0 0 0], 'ilimit', 5000, 'tol', 1e-3);
end;
%}


%{
moveto([0 0], qt(1,1),qt(1,2))

for i = 2 : pasos
    moveto(qt(i-1,:),qt(i,1),qt(i,2))
end;
%}

%{
N(1) = Link([0 0 10.5 0 0]);
N(2) = Link([0 0 18 pi 0]);
N(3) = Link([0 0 0 0 0]);

sc = SerialLink(N, 'name', 'scara');
sc.base = transl(21, 0, 0);

%T = sc.fkine([0 0 0]);

j = transl(1,1,0);
qf = sc.ikine(j,[0 0 0], [1 1 1 0 0 0], 'ilimit', 5000, 'tol', 1e-3 );

sc.plot([0 0 0]);

sc.teach;
%}